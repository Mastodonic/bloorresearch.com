<!doctype html>
<html class="no-js" lang="">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="apple-touch-icon.png">

    <!-- Critical CSS -->
    <style>
        <?php include('css/critical.css.php') ?>
    </style>

    <script src="scripts/modernizr-2.8.3.min.js"></script>
    <script>
    // include loadCSS here...
    function loadCSS(href, before, media) {
        "use strict";
        var ss = window.document.createElement("link");
        var ref = before || window.document.getElementsByTagName("script")[0];
        var sheets = window.document.styleSheets;
        ss.rel = "stylesheet";
        ss.href = href;
        ss.media = "only x";
        ref.parentNode.insertBefore(ss, ref);
        ss.onloadcssdefined = function(cb) {
            var defined;
            for (var i = 0; i < sheets.length; i++) {
                if (sheets[i].href && sheets[i].href === ss.href) {
                    defined = true;
                }
            }
            if (defined) {
                cb();
            } else {
                setTimeout(function() {
                    ss.onloadcssdefined(cb);
                });
            }
        };
        ss.onloadcssdefined(function() {
            ss.media = media || "all";
        });
        return ss;
    }
    // load a file
    loadCSS("css/main.dist.min.css");
    // loadCSS("css/final.css");
    </script>
    <noscript>
        <link href="css/main.dist.min.css" rel="stylesheet">
    </noscript>
</head>