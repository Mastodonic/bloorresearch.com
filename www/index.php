<!DOCTYPE html>
<!--[if IE 8]>      <html class="no-js lt-ie10 lt-ie9" lang="en"> <![endif]-->
<!--[if IE 9]>      <html class="no-js lt-ie10" lang="en"> <![endif]-->
<html class="no-js" lang="en"> <!--<![endif]-->

<head>
	<meta charset="utf-8">
	<title>Bloor Research</title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<link rel="apple-touch-icon" href="apple-touch-icon.png">
	<link href="css/main.css" rel="stylesheet">
	<script src="/scripts/modernizr.js"></script>
	<!--[if IE 8]> 	
		<script src="/scripts/respond.js"></script>
		<script src="/scripts/html5shiv.min.js"></script>
	<![endif]-->
</head>

<body>
	<div class="skip-links visuallyhidden">
		<a href="#main" title="Skip to main content">Skip to main content</a>
		<a href="#navigation-primary" title="Main site navigation">Main site navigation</a>
	</div>

	<?php //Global SVG Sprite  ?>
	<?php include_once('images/svg/svg-def.php'); ?>

	<div class="main-container">
		<?php //Header & Navigation ?>
		<?php include_once('includes/header.php'); ?>
		<main id="main">
			<section class="m-banner module jumbotron">
				<div class="grid-row">
					<div class="first">
						<h1>Welcome to Bloor</h1>
						<p>The best place to find research on Technology, Analytics, Security Analytics, Big Data and New Development.</p>
						<form class="search-form" action="#" method="get">
							<label class="visuallyhidden" for="search-field">Search</label>
							<input class="search-input" id="search-field" name="search" type="text" placeholder="Enter search term...">
							<button class="submit" type="submit">
								<svg>
									<use xlink:href="#shape-magnifying-glass"></use>
								</svg>
								<span class="visuallyhidden">Submit</span>
							</button>
						</form>
					</div>
					<div class="last">
						<h3>Popular on Bloor</h3>
						<ul>
							<li><a href="#">Getting to Actionable Insight</a></li>
							<li><a href="#">TECSYS - Getting an EASEy way forward with WMS</a></li>
							<li><a href="#">Black Monday - Supply Chain problems</a></li>
							<li><a href="#">Traditional value chains can't keep pace</a></li>
							<li><a href="#">Warehouse Management and the Mobile Supply Chain</a></li>
						</ul>
					</div>
				</div>
			</section>
			<section>
				<div class="m-promo module">
					<div class="grid-row">
						<article class="promo-item">
							<header>
								<h2>What&rsquo;s Innovative?</h2>
								<span>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</span>
								<svg class="icon" data-src="#shape-text">
									<use xlink:href="#shape-text"></use>
								</svg>
							</header>
							<div class="content">
								<div class="main">
									<a href="#" class="featured-image">
										<img src="/images/home-01.jpg" alt="What&rsquo;s Innovative?">										
									</a>
									<time datetime="2015-09-04">Sep 4, 2015</time>
									<h3>
										<a href="#">
											Apple Pay a boon to people with disabilities
										</a>
									</h3>
									<p>Lorem ipsum dolor sit amet, consectetur adipisci elit. Proin pellentesque et nunc vel interdum. Vivamus imperdiet congue ante, sodales accu...</p>								
								</div>
								<aside>
									<ul>
										<li>
											<a href="#">
												<time datetime="2015-09-04">Sep 4, 2015</time>
												<h4>
													<svg class="icon" data-src="#shape-arrow-right">
														<use xlink:href="#shape-arrow-right"></use>
													</svg>
													IBM's Recipes community
												</h4>
											</a>
										</li>
										<li>
											<a href="#">
												<time datetime="2015-09-02">Sep 2, 2015</time>
												<h4>
													<svg class="icon" data-src="#shape-arrow-right">
														<use xlink:href="#shape-arrow-right"></use>
													</svg>
													Oracle announce	new additions to Oracle Cloud Platform
												</h4>
											</a>
										</li>
										<li class="last-item">
											<a href="#">
												<time datetime="2015-10-04">Oct 4, 2015</time>
												<h4>
												<svg class="icon" data-src="#shape-arrow-right">
													<use xlink:href="#shape-arrow-right"></use>
												</svg>
												IBM's Recipes community</h4>												
											</a>
										</li>
									</ul>
								</aside>
							</div>
						</article>
						<article class="promo-item last">
							<header>
								<h2>What&rsquo;s Changed?</h2>
								<span>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</span>
								<svg class="icon" data-src="#shape-earth">
									<use xlink:href="#shape-earth"></use>
								</svg>
							</header>
							<div class="content">
								<div class="main">
									<a href="#" class="featured-image">
										<img src="/images/home-02.jpg" alt="What&rsquo;s Changed?">
									</a>
									<time datetime="2015-09-04">Sep 4, 2015</time>
									<h3>
										<a href="#">
											Apple Pay a boon to people with disabilities
										</a>
									</h3>
									<p>Lorem ipsum dolor sit amet, consectetur adipisci elit. Proin pellentesque et nunc vel interdum. Vivamus imperdiet congue ante, sodales accu...</p>								
								</div>
								<aside>
									<ul>
										<li>
											<a href="#">
												<time datetime="2015-09-04">Sep 4, 2015</time>
												<h4>
													<svg class="icon" data-src="#shape-arrow-right">
														<use xlink:href="#shape-arrow-right"></use>
													</svg>
													IBM's Recipes community
												</h4>
											</a>
										</li>
										<li>
											<a href="#">
												<time datetime="2015-09-02">Sep 2, 2015</time>
												<h4>
													<svg class="icon" data-src="#shape-arrow-right">
														<use xlink:href="#shape-arrow-right"></use>
													</svg>
													Oracle announce	new additions to Oracle Cloud Platform
												</h4>
											</a>
										</li>
										<li class="last-item">
											<a href="#">
												<time datetime="2015-10-04">Oct 4, 2015</time>
												<h4>
												<svg class="icon" data-src="#shape-arrow-right">
													<use xlink:href="#shape-arrow-right"></use>
												</svg>
												IBM's Recipes community</h4>												
											</a>
										</li>
									</ul>
								</aside>
							</div>
						</article>
					</div>
				</div>
			</section>
			<section>
				<div class="m-promo module grey">
					<div class="grid-row">
						<article class="promo-item">
							<header>
								<h2>Where&rsquo;s The Value?</h2>
								<span>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</span>
								<svg class="icon" data-src="#shape-money">
									<use xlink:href="#shape-money"></use>
								</svg>
							</header>
							<div class="content">
								<div class="main">
									<a href="#" class="featured-image">
										<img src="/images/home-03.jpg" alt="Where&rsquo;s The Value?">										
									</a>
									<time datetime="2015-09-04">Sep 4, 2015</time>
									<h3>
										<a href="#">
											Apple Pay a boon to people with disabilities
										</a>
									</h3>
									<p>Lorem ipsum dolor sit amet, consectetur adipisci elit. Proin pellentesque et nunc vel interdum. Vivamus imperdiet congue ante, sodales accu...</p>								
								</div>
								<aside>
									<ul>
										<li>
											<a href="#">
												<time datetime="2015-09-04">Sep 4, 2015</time>
												<h4>
													<svg class="icon" data-src="#shape-arrow-right">
														<use xlink:href="#shape-arrow-right"></use>
													</svg>
													IBM's Recipes community
												</h4>
											</a>
										</li>
										<li>
											<a href="#">
												<time datetime="2015-09-02">Sep 2, 2015</time>
												<h4>
													<svg class="icon" data-src="#shape-arrow-right">
														<use xlink:href="#shape-arrow-right"></use>
													</svg>
													Oracle announce	new additions to Oracle Cloud Platform
												</h4>
											</a>
										</li>
										<li class="last-item">
											<a href="#">
												<time datetime="2015-10-04">Oct 4, 2015</time>
												<h4>
												<svg class="icon" data-src="#shape-arrow-right">
													<use xlink:href="#shape-arrow-right"></use>
												</svg>
												IBM's Recipes community</h4>												
											</a>
										</li>
									</ul>
								</aside>
							</div>
						</article>
						<article class="promo-item last">
							<header>
								<h2>Expert Opinion.</h2>
								<span>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</span>
								<svg class="icon" data-src="#shape-person">
									<use xlink:href="#shape-person"></use>
								</svg>
							</header>
							<div class="content">
								<div class="main">
									<a href="#" class="featured-image">
										<img src="/images/home-04.jpg" alt="Expert Opinion.">										
									</a>
									<time datetime="2015-09-04">Sep 4, 2015</time>
									<h3>
										<a href="#">
											Apple Pay a boon to people with disabilities
										</a>
									</h3>
									<p>Lorem ipsum dolor sit amet, consectetur adipisci elit. Proin pellentesque et nunc vel interdum. Vivamus imperdiet congue ante, sodales accu...</p>								
								</div>
								<aside>
									<ul>
										<li>
											<a href="#">
												<time datetime="2015-09-04">Sep 4, 2015</time>
												<h4>
													<svg class="icon" data-src="#shape-arrow-right">
														<use xlink:href="#shape-arrow-right"></use>
													</svg>
													IBM's Recipes community
												</h4>												
											</a>
										</li>
										<li>
											<a href="#">
												<time datetime="2015-09-02">Sep 2, 2015</time>
												<h4>
													<svg class="icon" data-src="#shape-arrow-right">
														<use xlink:href="#shape-arrow-right"></use>
													</svg>
													Oracle announce	new additions to Oracle Cloud Platform
												</h4>
											</a>
										</li>
										<li class="last-item">
											<a href="#">
												<time datetime="2015-10-04">Oct 4, 2015</time>
												<h4>
												<svg class="icon" data-src="#shape-arrow-right">
													<use xlink:href="#shape-arrow-right"></use>
												</svg>
												IBM's Recipes community</h4>												
											</a>
										</li>
									</ul>
								</aside>
							</div>
						</article>
					</div>
				</div>
			</section>
		</main>
	</div>

	<?php //Chat Popup ?>
	<?php include_once('includes/chat-box.php') ?>

	<?php //Footer ?>
	<?php include_once('includes/footer.php') ?>
</body>

</html>
