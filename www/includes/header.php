<header class="main-header">
	<div class="section-canvas">
		<div class="top-bar">
			<div class="grid-row">
				<div class="login">
					<ul>
						<li><a href="#">Login</a></li>
						<li><a href="#">Register</a></li>
					</ul>
				</div>
			</div>
		</div>
		<nav class="navbar navbar-default">
			<div class="grid-row">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="#">
						<div class="logo">
							<img src="/images/bloor-logo.png" alt="Bloor Logo">
						</div>
					</a>
				</div>
				<nav class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul id="navigation-primary" class="main-nav nav navbar-nav">
						<li class="active has-children">
							<a href="#" class="toggle" role="button" aria-haspopup="true" aria-expanded="false">Trends</a>
							<div class="dropdown">
								<div class="canvas">
									<ul>
										<li><a href="#">Actionable Insight</a></li>
										<li><a href="#">Freedom</a></li>
										<li><a href="#">Big Data</a></li>
										<li><a href="#">Internet of Things</a></li>
										<li><a href="#">Cloud</a></li>
										<li><a href="#">Trust</a></li>
									</ul>
								</div>
							</div>
						</li>

						<li class="has-children">
							<a href="#" class="toggle" role="button" aria-haspopup="true" aria-expanded="false">Issues</a>
							<div class="dropdown">
								<div class="canvas">
									<ul>
										<li><a href="#">Accessibility</a></li>
										<li><a href="#">Cybercrime</a></li>
										<li><a href="#">Governance</a></li>
										<li><a href="#">Legacy Modernisation</a></li>
										<li><a href="#">Location</a></li>
										<li><a href="#">Solvency II</a></li>
									</ul>
								</div>
							</div>
						</li>
						<li class="has-children">
							<a href="#" class="toggle" role="button" aria-haspopup="true" aria-expanded="false">Technology</a>
							<div class="dropdown">
								<div class="canvas">
									<ul>
										<li><a href="#">Data</a></li>
										<li><a href="#">Process</a></li>
										<li><a href="#">Security</a></li>
										<li><a href="#">Other</a></li>
									</ul>
								</div>
							</div>
						</li>
						<li class="has-children companies-list">
							<a href="#" class="toggle" role="button" aria-haspopup="true" aria-expanded="false">Companies</a>
							<div class="dropdown">
								<div class="canvas">
									<div class="list-container">
										<div class="alphabetic-list">
											<ul>
												<li><a href="#">A</a></li>
												<li><a href="#">B</a></li>
												<li><a href="#">C</a></li>
												<li><a href="#">D</a></li>
												<li><a href="#">E</a></li>
												<li><a href="#">F</a></li>
												<li><a href="#">G</a></li>
												<li><a href="#">H</a></li>
												<li><a href="#">I</a></li>
												<li><a href="#">J</a></li>
												<li><a href="#">K</a></li>
												<li><a href="#">L</a></li>
												<li><a href="#">M</a></li>
												<li><a href="#">N</a></li>
												<li><a href="#">O</a></li>
												<li><a href="#">P</a></li>
												<li><a href="#">Q</a></li>
												<li><a href="#">R</a></li>
												<li><a href="#">S</a></li>
												<li><a href="#">T</a></li>
												<li><a href="#">U</a></li>
												<li><a href="#">V</a></li>
												<li><a href="#">W</a></li>
												<li><a href="#">X</a></li>
												<li><a href="#">Y</a></li>
												<li><a href="#">Z</a></li>								
											</ul>
											<a class="view-all" href="#">View All</a>
										</div>
										<div class="full-list">
											<ul>
												<li><a href="#">Atego</a></li>
												<li><a href="#">Acuno</a></li>
												<li><a href="#">Ataccama</a></li>
												<li><a href="#">Arbor Networks</a></li>
												<li><a href="#">Aero Scout</a></li>
												<li><a href="#">Blue Prism</a></li>
												<li><a href="#">BP Logix</a></li>
												<li><a href="#">Brainstorm</a></li>
												<li><a href="#">Bosch Software Innovation</a></li>
												<li><a href="#">Bizagi</a></li>
												<li><a href="#">CA Technologies</a></li>
												<li><a href="#">Cisco Systems</a></li>
												<li><a href="#">Courion</a></li>
											</ul>
										</div>
									</div>									
								</div>
							</div>
						</li>
						<li class="has-children">
							<a href="#" class="toggle" role="button" aria-haspopup="true" aria-expanded="false">Research</a>
							<div class="dropdown">
								<div class="canvas">
									<ul>
										<li><a href="#">eBooks</a></li>
										<li><a href="#">Product Evaluations</a></li>
										<li><a href="#">White Papers</a></li>
										<li><a href="#">Spotlights</a></li>
										<li><a href="#">Full Archive</a></li>
									</ul>
								</div>
							</div>
						</li>
						<li class="has-children">
							<a href="#" class="toggle" role="button" aria-haspopup="true" aria-expanded="false">Analysts</a>
							<div class="dropdown">
								<div class="canvas">
									<ul>
										<li><a href="#">Practice Leaders</a></li>
										<li><a href="#">Associate Analysts</a></li>
										<li><a href="#">View All</a></li>
									</ul>
								</div>
							</div>
						</li>
						<li class="login">
							<ul>
								<li><a href="#">Login</a></li>
								<li><a href="#">Register</a></li>
							</ul>
						</li>
					</ul>
				</nav>
			</div>
		</nav>
		<div class="bottom-bar">
			<div class="grid-row">
				<div class="time">
					<time datetime="2015-09-06">
						Sunday 6th September 2015
					</time>
					<span class="hour">
						<svg class="icon" data-src="#shape-clock">
							<use xlink:href="#shape-clock"></use>
						</svg>
						18:50 GMT
					</span>					
				</div>
				<div class="view-calendar">
					<a href="#">
						<svg class="icon" data-src="#shape-calendar">
							<use xlink:href="#shape-calendar"></use>
						</svg>
						<span>View our events calendar</span>
					</a>
				</div>
			</div>
		</div>
	</div>
</header>