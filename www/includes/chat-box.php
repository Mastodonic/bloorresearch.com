<div id="chat-box" class="m-chat-box module">
	<div class="action" role="button">
		<svg class="icon" data-src="#shape-bubble">
			<use xlink:href="#shape-bubble"></use>
		</svg>			
	</div>
	<div class="content">
		<p>Magnam iure impedit eum natus accusantium ut sapiente fuga voluptate sint amet? Unde aspernatur dicta assumenda eligendi laboriosam eum, perspiciatis et doloremque.</p>
	</div>
</div>