/* Main Javascript File */

var APP = APP || {};

APP.common = APP.common || (function() {
	function init() {
		svgFallback();
		handleChatbox();
	}

	function handleChatbox() {
		var $container = $('#chat-box');
		$container.on('click', '.action', function(){
			$container.toggleClass('active');
		});
	}

	function svgFallback() {
		if ($('html').hasClass('lt-ie10')) {
			$('.icon').each(function(){
				var $this = $(this),
					href = $(this).data('src'),
					img;
				if (href) {
					href = href.substring(1);
					img = '<img src="/images/' + href + '.png" class="icon" alt="">';
					$this.after(img);
					$this.detach();
				}
			});
		}
	}

	return {
		init: init
	};
}());

// Mobile navigation
APP.mobileNavigation = (function() {
	var $navigationToggle = $('#navigation-toggle'),
		$body = $('body');

	function init() {
		$navigationToggle.on('click', handleMenuToggle);
	}

	function handleMenuToggle(e) {
		e.preventDefault();
		if(!$body.hasClass('menu-open')) {
			$body.addClass('menu-open');
		} else {
			$body.removeClass('menu-open');
		}
	}
	return {
		init: init
	};

}());

$(document).ready(function() {
	APP.navigation.init();
	APP.common.init();
});
