/* Navigation Javascript File */

var APP = APP || {};

APP.navigation = APP.navigation || (function() {
    'use strict';
    var $body = $('body'),
        $nav = $('#navigation-primary'),
        $dropDown = $nav.find('.has-children'),
        $dropDownList = $dropDown.find('.dropdown'),
        speed = 250;

    function init() {
        //Get Window Width
        var ww = $(window).width();

        //Hide All Dropdowns on init
        $dropDownList.hide();

        //Hide subenus when navigaton is closed
        $('.navbar-toggle').on('click', handleClose);

            //Large Touch Devices
        if (ww <= 1400 && ww >= 1024 && Modernizr.touchevents) {
            $nav.on('touchstart click', '.toggle', handleHover);
        } else if (ww < 1024) {
            //Small Touch Devices
            $nav.on('click', '.toggle', handleDropDown);
        } else {
            $nav.on('mouseover', '.has-children', handleMouseover);
            $nav.on('mouseout', '.has-children', handleMouseout);
        }

        //Handle Menu on Resize
        $(window).on('resize', function() {
            ww = $(window).width();
            if (ww <= 1400 && ww >= 1024 && Modernizr.touchevents) {
                $nav.off('click');
                $nav.off('mouseover');
                $nav.off('mouseout');
                $nav.on('click', '.toggle', handleHover);
            } else if (ww < 1024) {
                //Small Touch Devices
                $nav.off('click');
                $nav.off('mouseover');
                $nav.off('mouseout');
                $nav.on('click', '.toggle', handleDropDown);
            } else {
                $nav.off('click');
                $nav.off('mouseover');
                $nav.off('mouseout');
                $nav.on('mouseover', '.has-children', handleMouseover);
                $nav.on('mouseout', '.has-children', handleMouseout);
            }
        });
    }
    //Desktop
    function handleMouseover() {
        var $this = $(this);
        $dropDown.removeClass('open');
        $this.addClass('open');
        $this.siblings().addClass('de-active');
        $body.addClass('menu-open');
        $this.find('.toggle').attr('aria-expanded', 'true');
    }

    function handleMouseout() {
        var $this = $(this);
        $body.removeClass('menu-open');
        $dropDown.removeClass('open');
        $this.siblings().removeClass('de-active');
        $this.find('.toggle').attr('aria-expanded', 'false');
    }

    //Tablet
    function handleHover(e) {
        var $this = $(this),
            $parent = $this.parent();

        e.preventDefault();
        if ($parent.hasClass('open')) {
            $body.removeClass('menu-open');
            $dropDown.removeClass('open');
            $parent.siblings().removeClass('de-active');
            $this.attr('aria-expanded', 'false');
        } else {            
            $dropDown.removeClass('open');
            $parent.addClass('open');
            $parent.siblings().addClass('de-active');
            $body.addClass('menu-open');
            $this.attr('aria-expanded', 'true');
        }
    }

    //Mobile
    function handleClose() {
        setTimeout(function() {
            $dropDown.removeClass('open');
            $dropDownList.hide();
        }, 300);
    }

    function handleDropDown(e) {
        var $this = $(this),
            $parent = $this.parent(),
            $list = $this.siblings('.dropdown');

        e.preventDefault();

        if ($parent.hasClass('open')) {
            $dropDown.removeClass('open');
            $dropDownList.slideUp(200);
            $this.attr('aria-expanded', 'false');
        } else {
            $dropDown.removeClass('open');
            $dropDownList.slideUp();
            $parent.addClass('open');
            $list.slideDown(speed);
            $this.attr('aria-expanded', 'true');
        }
    }

    return {
        init: init
    };
}());
