module.exports = function(grunt) {
    var path = require('path'),
        fs = require('fs'),
        mozjpeg = require('imagemin-mozjpeg'),
        config = grunt.file.readJSON('package.json'),
        $siteUrl = config.url;
    // 1. All configuration goes here

    grunt.initConfig({
        pkg: config,
        uglify: {
            dist: {
                options: {
                    preserveComments: false,
                    mangle: {
                        sort: true,
                        toplevel: true
                    },
                    sourceMap: false,
                    compress: {
                        sequences: true,
                        dead_code: true,
                        conditionals: true,
                        booleans: true,
                        unused: true,
                        if_return: true,
                        join_vars: true,
                        drop_console: false
                    }
                },
                files: {
                    'www/scripts/main.min.js': ['www/scripts/main.js']
                },
            },
        },
        concat: {
            options: {
                separator: ';'
            },
            dist: {
                src: ['frontend-source/js/vendor/*.js', 'frontend-source/js/main/*.js'],
                dest: 'www/scripts/main.js',
            }
        },
        copy: {
            js: {
                files: [{
                    expand: true,
                    dot: true,
                    cwd: 'frontend-source/js/head',
                    dest: 'www/scripts',
                    src: [
                        '{,*/}*.js'
                    ]
                }]
            },
        },
        browserSync: {
            dev: {
                bsFiles: {
                    src: ['www/css/main.css',
                        'www/**/main.js',
                        'www/**/*.html',
                        'www/**/*.cshtml',
                        'www/**/*.php',
                        'www/images/*.jpg',
                        'www/images/*.png',
                        'www/images/**/*.svg',
                    ]
                },
                options: {
                    watchTask: true,
                    proxy: $siteUrl
                }
            }
        },
        svgstore: {
            options: {
                prefix: 'shape-', // This will prefix each <g> ID
                svg: {
                    style: "display: none"
                }
            },
            default: {
                files: {
                    'www/images/svg/svg-def.php': ['frontend-source/graphics/sprite/*.svg']
                }
            }
        },

        imagemin: { // Task
            options: { // Target options
                optimizationLevel: 7,
                svgoPlugins: [{
                    removeViewBox: false
                }],
                use: [mozjpeg()]
            },
            dist: {
                files: [{
                    expand: true, // Enable dynamic expansion
                    cwd: 'frontend-source/graphics/watched', // Src matches are relative to this path
                    src: ['**/*.{jpg,gif,svg}'], // Actual patterns to match
                    dest: 'www/images' // Destination path prefix
                }]
            }
        },

        tinypng: {
            options: {
                apiKey: "W9quwfI25w-6dmReMEfFg0lq-ZvQsxC9",
                checkSigs: true,
                sigFile: 'frontend-source/graphics/file_sigs.json',
                summarize: true,
                showProgress: true,
                stopOnImageError: true
            },
            compress: {
                src: ['*.png'],
                cwd: 'frontend-source/graphics/watched',
                dest: 'www/images',
                expand: true
            }
        },

        sass: {
            dist: {
                options: {
                    sourceMap: true,
                    debugInfo: true
                },
                files: {
                    'www/css/main.css': 'frontend-source/scss/main.scss'
                }
            }
        },

        autoprefixer: {
            dist: {
                files: {
                    'www/css/main-ap.css': 'www/css/main.css'
                }
            }
        },



        cssmin: {
            options: {
                shorthandCompacting: false,
                roundingPrecision: -1
            },
            target: {
                files: {
                    'www/css/main.min.css': ['www/css/main-ap.css']
                }
            },            
            critical: {
                files: {
                    'www/css/critical.min.css': ['www/css/critical.css']
                }
            }
        },

        uncss: {
            dist: {
                files: {
                    'www/css/main-uncss.css': [$siteUrl]
                }
            }
        },
        penthouse: {
            extract: {
                outfile: './www/css/critical.css',
                css: './www/css/main.css',
                url: $siteUrl,
                width: 400,
                height: 800
            },
        },

        watch: {
            svg: {
                files: ['frontend-source/graphics/sprite/*.svg'],
                tasks: ['svgstore']
            },
            sass: {
                files: ['frontend-source/scss/**/*.scss'],
                tasks: ['sass:dist', 'autoprefixer:dist']
            },
            imagemin: {
                files: ['frontend-source/graphics/watched/*.{jpg,gif}'],
                tasks: ['imagemin']
            },
            tinypng: {
                files: ['frontend-source/graphics/watched/*.png'],
                tasks: ['tinypng']
            },
            concat: {
                files: ['frontend-source/js/main/*.js'],
                tasks: ['concat']
            },
            uglify: {
                files: ['www/scripts/main.js'],
                tasks: ['uglify']
            },
            copy: {
                files: ['frontend-source/js/head/*.js'],
                tasks: ['copy:js']
            }
        },
        bower: {
            install: {
                options: {
                    targetDir: './',
                    layout: function(type, component, source) {
                        var targetPath = type,
                            foundationFnction;
                            
                        if (component === 'jquery') {
                            targetPath = path.join('www', 'scripts');
                        } else if (type === 'js' && component !== 'jquery') {
                            targetPath = path.join('frontend-source', 'js', 'vendor');
                        } else if (type === '.css' || type === 'css') {
                            targetPath = path.join('frontend-source', 'scss', 'vendor');
                        } else if (type === '.scss' || type === 'scss') {
                            targetPath = path.join('frontend-source', 'scss', 'vendor');
                        } 

                        foundationFnction = source.indexOf("_functions");
                        if (type === 'scss' && foundationFnction == 44) {
                            targetPath = path.join('frontend-source', 'scss');
                        }

                        return targetPath;
                    },
                    install: true,
                    verbose: true,
                    cleanTargetDir: false,
                    cleanBowerDir: false
                }
            }
        }
    });


    // Loading Npm Modules
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-sass');
    grunt.loadNpmTasks('grunt-svgstore');
    grunt.loadNpmTasks('grunt-contrib-imagemin');
    grunt.loadNpmTasks('grunt-tinypng');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-browser-sync');
    grunt.loadNpmTasks('grunt-autoprefixer');
    grunt.loadNpmTasks('grunt-penthouse');
    grunt.loadNpmTasks('grunt-uncss');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-bower-task');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-clean');

    //Grunt Tasks
    grunt.registerTask('install', ['bower', 'copy','clean']);
    grunt.registerTask('build-img', ['imagemin', 'svgstore', 'tinypng']);
    grunt.registerTask('build-critical', ['penthouse','cssmin:critical']);
    grunt.registerTask('build', ['copy', 'concat', 'uglify', 'sass', 'autoprefixer', 'cssmin']);
    grunt.registerTask('default', ['build','browserSync', 'watch']);
};
